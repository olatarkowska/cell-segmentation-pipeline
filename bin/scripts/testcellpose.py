from cellpose import models, utils, plot
import skimage.io
import numpy as np
import glob
from pathlib import Path
import tqdm
import os
import tifffile as tf
import sys
import argparse
# to measure exec time
from timeit import default_timer as timer
import mxnet as mx


def run_cellpose_3D(args):
    # check if GPU working, and if so use it
    use_gpu = utils.use_gpu()
    if use_gpu:
        device = mx.gpu()
    else:
        device = mx.cpu()
    print("device", device)

    os.environ['MXNET_CUDNN_AUTOTUNE_DEFAULT'] = '2'
    model = models.Cellpose(device, model_type='cyto')
    files = glob.glob(args.input.name)
    for f in tqdm.tqdm(files):
        img = skimage.io.imread(f)
        img = np.transpose(img, (1, 2, 3, 0))
        mask, flow, style, diam = model.eval(
           img, diameter=40, channels=[0, 1], do_3D=True
        )
        skimage.io.imsave(args.output.name, mask)


def run_cellpose_2D(args):
    # check if GPU working, and if so use it
    use_gpu = utils.use_gpu()
    if use_gpu:
        device = mx.gpu()
    else:
        device = mx.cpu()
    print("device", device)
    
    os.environ['MXNET_CUDNN_AUTOTUNE_DEFAULT'] = '2'
    model = models.Cellpose(device, model_type='cyto')
    files = glob.glob(args.input.name)
    for f in tqdm.tqdm(files):
        img = np.load(f, allow_pickle=True)
        #img = skimage.io.imread(f).squeeze()
        #img = np.transpose(img, (1, 2, 3, 0))
        masks, flows, styles, diams = model.eval(
            [img], diameter=40, channels=[0, 1]
        )
        skimage.io.imsave(args.output.name, masks[0])




if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument(
        '-in', '--input', type=argparse.FileType('r'), default=sys.stdin,
        required=True
    )
    parser.add_argument(
        '-out', '--output', type=argparse.FileType('w'), default=sys.stdout,
        required=True
    )

    parser.add_argument('--3D', dest='method', action='store_true')
    parser.add_argument('--2D', dest='method', action='store_false')
    #parser.set_defaults(method=False)
    args = parser.parse_args()

    start = timer()
    if args.method:
        run_cellpose_3D(args)
        print("with GPU (3D):", timer()-start)
    else:
        run_cellpose_2D(args)
        print("with GPU (2D):", timer()-start)

