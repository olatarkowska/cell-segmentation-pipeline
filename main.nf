#!/usr/bin/env nextflow

//params.hardware = "$baseDir/machineines.csv"
hardware = params.hardware
output_dir = file(params.output)

Channel
    .fromPath(hardware)
    .splitCsv(header:true)
    .map{ row -> 
      def machine = row.machine
      def gpu_no = row.gpu_no
      def gpu_type = row.gpu_type
      def nd = row.nd
      def in_file = file(row.in_file)
      tuple(machine, gpu_no, gpu_type, nd, in_file)
    }
    .set { ch_benchmark }

process cell_segmentation {

    label "cellpose"

    executor "google-lifesciences"

    input:
    path testcellpose from "${baseDir}/bin/scripts/testcellpose.py"
    set machine, gpu_no, gpu_type, nd, file(in_file) from ch_benchmark

    output:
    file "${result}_driver.log"
    file "${result}_smi.txt"
    file "${result}_out.png"
    file "${result}_gputime.txt"

    publishDir path: "${output_dir}", mode: "copy", saveAs: { filename -> "${filename}" }
    stageInMode 'copy'

    machineType "${machine}"
    accelerator = [request: "${gpu_no}", type: "${gpu_type}"]
    //accelerator ${gpu_no}, type: "${gpu_type}"

    shell:
    result = "${machine}_${gpu_no}_${gpu_type}"
    """
    echo $PATH
    nvidia-smi >"!{result}_smi.txt"
    cp /usr/local/nvidia/nvidia-installer.log "!{result}_driver.log"
    #source activate cellpose
    python !{testcellpose} --input !{in_file} --output "!{result}_out.png" --!{nd} >"!{result}_gputime.txt"
    """

}
